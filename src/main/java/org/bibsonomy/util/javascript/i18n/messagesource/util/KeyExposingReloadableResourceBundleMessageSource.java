package org.bibsonomy.util.javascript.i18n.messagesource.util;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * a {@link ReloadableResourceBundleMessageSource} that allows to retrieve messages
 * where the code starts with a prefix
 * 
 * @author dzo
 */
public class KeyExposingReloadableResourceBundleMessageSource extends ReloadableResourceBundleMessageSource {

	/**
	 * @param code
	 * @param args
	 * @param locale
	 * @return the messagees starting with key
	 */
	public Map<String, String> getMessagesStartingWithKey(String code, Object[] args, Locale locale) {
		final Map<String, String> messages = new HashMap<String, String>();
		
		final Set<Object> keySet = getMergedProperties(locale).getProperties().keySet();
		
		for (final Object possibleKey : keySet) {
			if (possibleKey instanceof String) {
				final String key = (String) possibleKey;
				
				if (key.startsWith(code)) {
					final String message = this.getMessage(key, args, locale);
					messages.put(key, message);
				}
			}
		}
		
		return messages;
	}
}
