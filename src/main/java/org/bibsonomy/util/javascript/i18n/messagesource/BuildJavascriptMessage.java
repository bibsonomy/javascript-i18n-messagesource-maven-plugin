package org.bibsonomy.util.javascript.i18n.messagesource;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.bibsonomy.util.javascript.i18n.messagesource.model.MessageKey;
import org.bibsonomy.util.javascript.i18n.messagesource.util.JSONUtils;
import org.bibsonomy.util.javascript.i18n.messagesource.util.KeyExposingReloadableResourceBundleMessageSource;
import org.codehaus.plexus.util.Scanner;
import org.sonatype.plexus.build.incremental.BuildContext;

/**
 * main class for building the tagx file
 * TODO: support javascript files
 *
 * @author dzo
 */
@Mojo(name = "buildMessageFiles")
public class BuildJavascriptMessage extends AbstractMojo {
	
	private static final Pattern ARG_STRING_PATTERN = Pattern.compile("^[\"']([0-9\\.\\_a-zA-Z]+)[\"']$");
	private static final Pattern ARG_STRING_VAR_PATTERN = Pattern.compile("^[\"']([0-9\\.\\_a-zA-Z]+)[\"']\\s*\\+\\s*(.+)$");
	
	@Component
	private BuildContext context;

	@Parameter(defaultValue = "getString")
	private String i18nMethodName;
	
	@Parameter(defaultValue = "LocalizedStrings")
	private String messageVarName;
	
	@Parameter
	private String[] locales = new String[] { Locale.ENGLISH.getLanguage() };

	@Parameter
	private String[] messageSources;

	@Parameter(defaultValue = "${project.basedir}/src/main/webapp")
	private File baseDirectory;
	
	@Parameter(defaultValue = "${project.build.directory}/javascript-i18n/messages.tagx")
	private File outputFile;

	@Parameter
	private String[] includes = new String[] { "**/*.js" };

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.maven.plugin.Mojo#execute()
	 */
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().info("Building JavaScript i18n file.");
		
		// build the pattern to look for
		final Pattern i18nMethodNamePattern = Pattern.compile(this.i18nMethodName + "\\((.+?)(,.+)?\\)");
		
		// find all files to look at
		final Scanner scanner = this.context.newScanner(this.baseDirectory, true);
		scanner.setIncludes(this.includes);
		scanner.scan();
		final String[] filesToScan = scanner.getIncludedFiles();
		
		final Set<MessageKey> allKeysInFiles = new HashSet<MessageKey>();
		for (String fileToScan : filesToScan) {
			try {
				final Set<MessageKey> keysInFile = extractCalls(fileToScan, i18nMethodNamePattern);
				allKeysInFiles.addAll(keysInFile);
			} catch (IOException e) {
				throw new MojoExecutionException("error reading file " + fileToScan, e);
			}
		}
		
		final KeyExposingReloadableResourceBundleMessageSource messageSource = new KeyExposingReloadableResourceBundleMessageSource();
		messageSource.setBasenames(this.messageSources);
		messageSource.setFallbackToSystemLocale(false);
		final Map<String, String> locationMaps = new HashMap<String, String>();
		for (final String language : this.locales) {
			final Locale locale = new Locale(language);
			final StringBuilder builder = new StringBuilder();
			final Iterator<MessageKey> iterator = allKeysInFiles.iterator();
			while (iterator.hasNext()) {
				final MessageKey messageKey = iterator.next();
				if (!messageKey.isRegex()) {
					final String key = messageKey.getKey();
					final String message = messageSource.getMessage(key, null, locale);
					appendMessage(builder, key, message);
				} else {
					final Map<String, String> messages = messageSource.getMessagesStartingWithKey(messageKey.getKey(), null, locale);
					final Iterator<Entry<String, String>> entrySetIterator = messages.entrySet().iterator();
					while (entrySetIterator.hasNext()) {
						Map.Entry<String, String> messageKeyValue = entrySetIterator.next();
						final String key = messageKeyValue.getKey();
						final String message = messageKeyValue.getValue();
						
						this.appendMessage(builder, key, message);
						
						if (entrySetIterator.hasNext()) {
							builder.append(",");
						}
					}
				}
				if (iterator.hasNext()) {
					builder.append(",");
				}
			}
			locationMaps.put(language, builder.toString());
		}
		
		if (!this.outputFile.exists()) {
			if (!this.outputFile.isFile()) {
				this.outputFile.getParentFile().mkdirs();
			} else {
				this.outputFile.mkdirs();
			}
		}
		
		// TODO: try with resource
		try {
			final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.outputFile), "UTF-8"));
			writer.write("<jsp:root version=\"2.0\" xmlns=\"http://www.w3.org/1999/xhtml\"\n" + 
					"	xmlns:jsp=\"http://java.sun.com/JSP/Page\"\n" + 
					"	xmlns:fn=\"http://java.sun.com/jsp/jstl/functions\"\n" + 
					"	xmlns:fmt=\"http://java.sun.com/jsp/jstl/fmt\"\n" + 
					"	xmlns:c=\"http://java.sun.com/jsp/jstl/core\">\n" + 
					"	\n" + 
					"	<jsp:directive.attribute name=\"locale\" type=\"java.util.Locale\" required=\"true\"/>\n");
			
			for (final String langauge : this.locales) {
				writer.write("\n<c:if test=\"${locale.language eq '" + langauge + "' }\">\n" + 
						"		<script type=\"text/javascript\">\n" + 
						"			<![CDATA[\n" +
						"			var " + this.messageVarName + " = {" + locationMaps.get(langauge) + "}\n" +
						"			]]>" + 
						"		</script>\n" + 
						"	</c:if>");
			}
			
			writer.write("</jsp:root>");
			writer.close();
		} catch (IOException e) {
			throw new MojoExecutionException("writing output file failed.", e);
		}
	}

	/**
	 * @param builder
	 * @param key
	 * @param message
	 */
	private void appendMessage(final StringBuilder builder, final String key, final String message) {
		builder.append("\"" + JSONUtils.quoteJSON(key) + "\":\"" + JSONUtils.quoteJSON(message) + "\"");
		
		this.getLog().debug(key + " = " + message);
	}

	/**
	 * @param pattern
	 * @param fileToScan
	 * @return
	 */
	private Set<MessageKey> extractCalls(final String fileToScan, final Pattern pattern) throws IOException {
		final Set<MessageKey> keysInFile = new HashSet<MessageKey>();
		final File file = new File(this.baseDirectory, fileToScan);
		// TODO: use try with resource
		final BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		this.getLog().debug("scanning file " + fileToScan);
		while ((line = reader.readLine()) != null) {
			final Matcher matcher = pattern.matcher(line);
			while (matcher.find()) {
				this.getLog().debug("found " + matcher.group() + " in file " + fileToScan);
				final String keyArgument = matcher.group(1);
				final Matcher argMatcher = ARG_STRING_PATTERN.matcher(keyArgument);
				if (argMatcher.find()) {
					final String key = argMatcher.group(1);
					keysInFile.add(new MessageKey(key));
					this.getLog().debug("added key '" + key + "'");
				} else {
					final Matcher stringArgMatcher = ARG_STRING_VAR_PATTERN.matcher(keyArgument);
					if (stringArgMatcher.find()) {
						final String key = stringArgMatcher.group(1);
						keysInFile.add(new MessageKey(key, true));
						this.getLog().info("Including all '" + key + ".*' for key '" + key + "'.");
					}
				}
			}
		}
		reader.close();
		return keysInFile;
	}

}
