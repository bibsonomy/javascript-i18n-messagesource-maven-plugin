package org.bibsonomy.util.javascript.i18n.messagesource.model;

/**
 * class representing message key informations
 *
 * @author dzo
 */
public class MessageKey {
	private String key;
	private boolean regex;
	
	/**
	 * default constructor
	 * sets regex false
	 * @param key
	 */
	public MessageKey(final String key) {
		this(key, false);
	}
	
	/**
	 * @param key
	 * @param regex
	 */
	public MessageKey(String key, boolean regex) {
		this.key = key;
		this.regex = regex;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return this.key;
	}
	
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * @return the regex
	 */
	public boolean isRegex() {
		return this.regex;
	}
	/**
	 * @param regex the regex to set
	 */
	public void setRegex(boolean regex) {
		this.regex = regex;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + (regex ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		MessageKey other = (MessageKey) obj;
		if (key == null) {
			if (other.key != null) return false;
		} else if (!key.equals(other.key)) return false;
		if (regex != other.regex) return false;
		return true;
	}
}
